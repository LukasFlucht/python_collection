from pyModbusTCP.client import ModbusClient
import time
import struct

SERVER_HOST = "10.20.203.110"
SERVER_PORT = 502

c = ModbusClient()

c.host(SERVER_HOST)
c.port(SERVER_PORT)

pull = False

voltage_dic = {'L12':0,'L23':0,'L13':0}

while True:

    if not c.is_open():
        if not c.open():
            print("unable to connect to "+SERVER_HOST+":"+str(SERVER_PORT))

    if c.is_open():
        voltage_12 = c.read_holding_registers(7,2) #looks like [17450,50214]
        voltage_23 = c.read_holding_registers(9,2)
        voltage_13 = c.read_holding_registers(11,2)
        if voltage_12:
            voltage1 = voltage_12[0].to_bytes(2,byteorder = "little") #looks like b"0xcf3\C"
            voltage2 = voltage_12[1].to_bytes(2,byteorder = "little")
            concated_voltage1 = voltage2 + voltage1
            voltage1 = struct.unpack("f",concated_voltage1) #looks like 499.000
            voltage1_round = int(voltage1[0])

            voltage3 = voltage_23[0].to_bytes(2,byteorder = "little")
            voltage4 = voltage_23[1].to_bytes(2,byteorder = "little")
            concated_voltage2 = voltage4 + voltage3
            voltage2 = struct.unpack("f",concated_voltage2)
            voltage2_round = int(voltage2[0])

            voltage5 = voltage_13[0].to_bytes(2,byteorder = "little")
            voltage6 = voltage_13[1].to_bytes(2,byteorder = "little")
            concated_voltage3 = voltage6 + voltage5
            voltage3 = struct.unpack("f",concated_voltage3)
            voltage3_round = int(voltage3[0])


            if "L12" not in voltage_dic:
                voltage_dic["L12"] = 0
            else:
                voltage_dic["L12"] = voltage1_round

            if "L23" not in voltage_dic:
                voltage_dic["L23"] = 0
            else:
                voltage_dic["L23"] = voltage2_round

            if "L13" not in voltage_dic:
                voltage_dic["L13"] = 0
            else:
                voltage_dic["L13"] = voltage3_round

            for volt in voltage_dic:
                print(volt, "has a Voltage of" , voltage_dic[volt],"V")
    print('\n')
    time.sleep(5)

