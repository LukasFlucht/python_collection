import sys, urllib.request

#URL  Open und in Datei einlesen
try:
    u = urllib.request.urlopen \
                                        ("http://localhost/python36/url_lesen.htm")     #("google.de")
    print("readdone")
except:
    print("Fehler")
    sys.exit(0)

#Inhalt der URL  Local Kopieren 
urllib.request.urlretrieve \
                                     ("http://localhost/python36/url_lesen.htm",
                                       "C:/temp/url_kopieren.htm")
                           
# URL inhalt in liste schreiben
li = u.readlines()
print("inlist now ")

# URL schließen
u.close()
print("Closed")


for element in li:
    print(element)

    
