import math
'''
#21 Question
'''
points = {'UP':5,'DOWN':2,'RIGHT':3,'LEFT':2}
X = 0
Y = 0

for point in points:
    if point == 'UP':
        Y = Y + points[point]
    elif point == 'DOWN':
        Y = Y - points[point]
    elif point == 'RIGHT':
        X = X + points[point]
    elif point == 'LEFT':
        X = X - points[point]

point = round(math.sqrt(Y**2+X**2))

print('#21')
print('PointX = {} and PointY = {}'.format(Y,X))
print('The Squareroute is {}'.format(point))
print('\n')



'''
#22 Question
'''
str22 = 'New New to Python or choosing between Python 2 and Python 3? Read Python 2 or Python 3.'
str22lst = str22.split(' ')
str22dic = {}
counter = 0
for word in str22lst:
    if word in str22dic:
        str22dic[word] = str22dic[word] + 1
    else:
        str22dic[word] = 1

print('#22')
print(str22dic)
print('\n')



'''
#23 Question
'''
def square(x):
    return x**2

print('#23')
print(square(5))
print('\n')



'''
#24 Question
'''
def square(x):
    '''
    This Function takes a number abd Returns the Squareroute
    '''
    return x**2

print('#24')
print(square.__doc__)
print('\n')


'''
#25 Question
'''
class Person:
    def __init__(self,name):
        self.name = name


peter = Person('Peter')

print('#25')
print(peter.name)
print('\n')

'''
#26 Question
'''
def addition(a,b):
    return a +b

print('#26')
print(addition(3,4))
print('\n')


'''
#27 Question
'''
def convertToString(i):
    return str(i)

print('#27')
print(convertToString(3))
print('\n')



'''
#30 Question
'''
def concatString(a,b):
    return a + b

print('#30')
print(concatString('Hallo mein Name ist',' Lukas'))
print('\n')


'''
#31 Question
'''
str31a = 'Hello my name is Lukas1'

str31b = 'Hello my name is Luisa'

def checkstrlen(a='',b=''):
    if len(a) > len(b):
        return a
    elif len(b) > len(a):
        return b
    else:
        return a +' - '+ b

result31 = checkstrlen(str31a,str31b)

print('#31')
print(result31)
print('\n')



'''
#32 Question
'''
def evenorodd(i):
    if i % 2 == 0:
        return 'Even'
    else:
        return 'ODD'

numbertocheck = 10
result32 = evenorodd(numbertocheck)

print("#32")
print('{} is {}'.format(numbertocheck,result32))
print("\n")


'''
#33 Question
'''
def squaredic(i):
    dic = {}
    for num in range(0,i):
        dic[num] = num**2
    return dic

print("#33")
print(squaredic(10))
print("\n")



'''
#36 Question
'''
def squarelst(i):
    lst =[]
    for num in range(0,i):
        lst.append(num**2)
    return lst

print("#35")
print(squarelst(20))
print("\n")



'''
#37 Question
'''
def squarelst(i):
    lst =[]
    for num in range(0,i):
        lst.append(num**2)
    return lst[0:5]

print("#37")
print(squarelst(20))
print("\n")



'''
#41 Question
'''
tpl41 = (1,2,3,4,5,6,7,8,9,10)

print("#41")
print(tpl41[:5])
print(tpl41[5:])
print("\n")


'''
#42 Question
'''
tpl42 = (1,2,3,4,5,6,7,8,9,10)
lst42 = []
for num in tpl41:
    if num % 2 == 0:
        lst42.append(num)
    tuple(lst42)

print("#42")
print(lst42)
print("\n")


'''
#44 Question
'''
lst43 = [1,2,3,4,5,6,7,8,9,10]

lst43even = list(filter(lambda x : x % 2 ==0,lst43))

print("#44")
print(lst43even)
print("\n")


'''
#45 Question
'''
lst45 = [1,2,3,4,5,6,7,8,9,10]

lst45map = list(map(lambda x:x**2,lst45))

print("#45")
print(lst45map)
print("\n")


'''
#46 Question
'''
lst46 = [1,2,3,4,5,6,7,8,9,10]

lst46map = map(lambda x:x**2,lst46)

lst46flt = list(filter(lambda x:x%2==0,lst46map))

print("#46")
print(lst46flt)
print("\n")


'''
#47 Question
'''
evennum47 = list(filter(lambda x:x % 2 ==0,range(0,20)))

print("#47")
print(evennum47)
print("\n")



'''
#49 Question
'''
class person:
    '''
    persons with different behaviors
    '''
    def __init__(self,nation, behavior = "nutral"):
        self.nation = nation
        self.behavior = behavior

    def changebehavior(self,behavior):
        '''
        This Function is changing the behavior of the character
        '''
        self.behavior = behavior

american = person("American")

print("#49")
print(american.nation)
print("\n")


'''
#50 Question
'''
class badperson(person):
    def __init__(self,nation,behavior = "Bad"):
        super().__init__(nation,behavior)

badamerican = badperson("badAmerican")

print("#50")
print(badamerican.behavior)
print("\n")

class goodperson(badperson):
    def __init__(self,nation,behavior = "Good"):
        super().__init__(nation,behavior)


goodamerican = goodperson("badAmerican")

print("#50.1")
print(goodamerican.behavior)
print("\n")

goodamerican.changebehavior("Sucks")

print("#50.2")
print(goodamerican.behavior)
print("\n")


print(person.__doc__)
print(person.changebehavior.__doc__)

'''
#51 Question
'''
import math
class circle:
    def __init__(self, radius=0, area=0):
        self.radius = radius
        self.area = area

    def calcarea(self):
        self.area = math.pi * self.radius **2



kreis1 = circle(radius = 5)

kreis1.calcarea()

print("#50")
print(kreis1.area)
print("\n")


'''
#52 Question Rectangle
'''
class rectangle:
    def __init__(self,a,b,area=0):
        self.a = a
        self.b = b
        self.area = area

    def calcarea(self):
        self.area = self.a**2 * self.b**2

rectangle1 = rectangle(a=2,b=2)

print('#52')
print(rectangle1.area)

rectangle1.calcarea()

print(rectangle1.area)
print('\n')


'''
#53 subclass
'''
class shape:
    def __init__(self):
        pass
    def area(self):
        return 0

class square(shape):
    def __init__(self,l):
        shape.__init__(self)
        self.length = l

    def area(self):
        return self.length * self.length

asquare = square(3)
print(asquare.area())



'''
#54 subclass
'''
