VOWEL_COST = 250
LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
VOWELS = 'AEIOU'

# PART A: WOFPlayer
class WOFPlayer:
    def __init__(self,name,prizeMoney = 0,prizes= []):
        self.name = name
        self.prizeMoney = prizeMoney
        self.prizes = prizes [:]
        
    def addMoney(self,amt):
        self.prizeMoney = self.prizeMoney + amt
        
    def goBankrupt(self):
        self.prizeMoney = 0
        
    def addPrize(self,prize):
        self.prizes.append(prize)
    
    def __str__(self):
        return "{} (${})".format(name,prizeMoney)
    
# PART B: WOFHumanPlayer
class WOFHumanPlayer(WOFPlayer):
    def __init__(self,name,prizeMoney = 0,prizes= []):
        WOFPlayer.__init__(self,name,prizeMoney = 0,prizes= [])
    
    def __str__(self):
        return "{} (${})".format(name,prizeMoney)
    
    def getMove(self,category, obscuredPhrase, guessed):
        print("{} has ${}".format(self.name,self.prizeMoney))
        print("\n")
        print(category)
        print(obscuredPhrase)
        print(guessed)
      
        move = input("Guess a letter, phrase, or type 'exit' or 'pass':")
        return move
    
# PART C: WOFComputerPlayer
class WOFComputerPlayer(WOFPlayer):
    
    def __init__(self,name,difficulty,prizeMoney = 0,prizes= [],SORTED_FREQUENCIES = 'ZQXJKVBPYGFWMUCLDRHSNIOATE'):
        WOFPlayer.__init__(self,name,prizeMoney = 0,prizes= [])
        self.SORTED_FREQUENCIES = SORTED_FREQUENCIES
        self.difficulty = difficulty
        
    def getMove(self,category, obscuredPhrase, guessed):
        print("{} has ${}".format(self.name,self.prizeMoney))
        print("\n")
        print(category)
        print(obscuredPhrase)
        print(guessed)
      
        move = input("Guess a letter, phrase, or type 'exit' or 'pass':")
        return move
        
        
    def __str__(self):
        return "{} (${})".format(name,prizeMoney)

x = WOFComputerPlayer("Lukas",5)

print(x.SORTED_FREQUENCIES)

