import time

while True:

    localtime = time.localtime()

    sec = localtime[5]
    min = localtime[4]
    std = localtime[3]

    year = localtime[0]
    month = localtime[1]
    day = localtime[2]

    print('Date: {}-{}-{} \nTime: {}:{}:{}'.format(year,month,day,std,min,sec))

    time.sleep(2)
