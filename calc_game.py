def calc_game(a):
        import time
        import random
        random.seed()

        #Open Text File
        datei = open("Test.txt","w")

        #Time
        lt = time.localtime()

        jahr,monat,tag= lt[0:3]
        stunden,minuten,sekunden = lt[3:6]
        wochentagnr = lt[6]
        jahrestag = lt[7]
        sommerzeit = lt[8]

        wochentag = ['montag','dienstag','mitwoch','donnerstag','freitag','samstag','sonntag']

        if sommerzeit == 1:
            active = 'sommer zeit ist activ'
        if sommerzeit == 0:
            active = 'sommer zeit ist nicht a activ'

        print('Zeitstempel beginn des spiels:')
        print(jahr,'-',monat,'-',tag)
        print(stunden,':',minuten,':',sekunden)
        print(wochentag[wochentagnr])
        print('Tag de jahres:',jahrestag)
        print(active,'\n')

        richtig = int(0)
        falsch= int(0)
        i = int(0)

        #Game
        start_time = time.time()
        for i in range (1,a):
            a = random.randint(1,30)
            b = random.randint(1,30)
            c = a+b
            print(a,'+',b ,'= ?:')
            erg = int(input("Gebe dein ergebnis ein="))
            if c == erg:
                print("Ergebniss ist richtig",'\n')
                #writelog
                timestring = time.strftime ('%Y/%m/%d-%H:%M:%S')
                datei .write('%s ' % timestring )
                datei .write('Richtige Antwort wurde eingegeben fuer Aufgabe')
                datei.write(' %s\n ' % i)
                richtig = richtig +1
            else:
                print("Ergeniss ist Falsch",'\n')
                #Writelog
                timestring = time.strftime ('%Y/%m/%d-%H:%M:%S')
                datei .write('%s ' % timestring )
                datei .write('Falsche Antwort wurde eingegeben fuer Aufgabe')
                datei.write(' %s\n ' % i)
                falsch = falsch +1

        #Check Used Time in Seconds     
        stop_time = time.time()
        dif_time = stop_time - start_time
        dif_timesec = round(dif_time,4)
        dif_timemin = dif_timesec /60
        dif_timeminr = round(dif_timemin,2)

        print('Du hast',richtig,'richtig beantwortet und',falsch,'falsch beantwortet')

        if dif_timemin >= 1:
            print("Benötigte Zeit:")
            print(dif_timeminr,'Minuten',)
            print(dif_timesec,'Sekunden',)
        else:
            print("Du hast",dif_timesec,'Sekunden benötigt')

        #Writelog
        timestring = time.strftime ('%Y/%m/%d-%H:%M:%S')
        datei .write('%s ' % timestring )
        datei .write('Programm wurde bendet')

        datei.close()
