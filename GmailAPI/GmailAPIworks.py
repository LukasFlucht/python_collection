from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']


def getmessagelst():
    """Shows basic usage of the Gmail API.
    Lists the user's Gmail labels.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)

        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('gmail', 'v1', credentials=creds)
    # Call the Gmail API
    results = service.users().messages().list(userId='me').execute()
    return results


def getmsg(msgid):
    """Shows basic usage of the Gmail API.
    Lists the user's Gmail labels.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('gmail', 'v1', credentials=creds)

    # Call the Gmail API
    #results = service.users().messages().list(userId='me').execute()
    results = service.users().messages().get(userId='me', id=msgid).execute()
    return results

def markasread(msgid):
        creds = None
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('gmail', 'v1', credentials=creds)

        # Call the Gmail API
        #results = service.users().messages().list(userId='me').execute()
        results = service.users().messages().modify(userId = "me", id=msgid, body={'removeLabelIds': ['UNREAD']})
        print("massage {} is marked as Read".format(msgid))
        return


def getinbox():
    newmsg = 0
    inboxcounter = 0

    for msg in msglst['messages']:
        msgdic = getmsg(msg['id'])
        lbs = msgdic.get('labelIds')
        if 'INBOX' in lbs:
            print(msgdic)
            inboxcounter = inboxcounter +1
        if "UNREAD" in lbs:
            newmsg = newmsg =+1
            print(msgdic)
    print("You have {} new Massages".format(newmsg))


def gettext(msgorder):
    text = msgorder['snippet']
    return text

def getname(msgdic):
    namehole = msgdic["payload"]["headers"][15]["value"]
    namelst = namehole.split(" ")
    return " ".join(namelst[0:2])

def getorders():
    orders = 0
    for msg in msglst['messages']:
        msgdic = getmsg(msg['id'])
        lbs = msgdic.get('labelIds')
        if 'Label_4958894722248074314' and "UNREAD" in lbs:
            orders = orders + 1
            text = gettext(msgdic)
            name = getname(msgdic)
            orderfile = open("C:\GIT\Python_Collection\GmailAPI\Orders.txt", "a")
            orderfile.write(name + ": ")
            orderfile.write(text)
            orderfile.write("\n")
            orderfile.close()
            markasread(msg['id'])
    print("You have {} new orders".format(orders))


msglst = getmessagelst()
getinbox()
getorders()
