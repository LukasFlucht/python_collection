#Import Libarys
import RPI.GPIO as GPIO
import time

#GPIO Settings
#Controller over PGIONumber 
GPIO.setmode(GPIO.BCM)

#IN and OUTPUT Settings
GPIO.setup(5, GPIO.IN)
GPIO.setup(6, GPIO.OUT)

#MAIN
#Set Output While Switch is True
while True:
    if GPIO.Input1(5) == 0:
        GPIO.Output1(6,GPIO.HIGH)
    else:
        GPIO.Output1(6,GPIO.LOW)
