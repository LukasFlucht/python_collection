# import statements
#import requests_with_caching
import requests
import requests_cache
import json
import webbrowser

flickr_key = '1da1ae63aa6a3c84d639a56b2c20f25d'

def get_flickr_data(tags_string):
    baseurl = "https://api.flickr.com/services/rest/"
    params_diction = {}
    params_diction["api_key"] = flickr_key
    params_diction["tags"] = tags_string
    params_diction["tag_mode"] = "all"
    params_diction["method"] = "flickr.photos.search"
    params_diction["per_page"] = 1
    params_diction["media"] = "photos"
    params_diction["format"] = "json"
    params_diction["nojsoncallback"] = 1
    flickr_resp = requests.get(baseurl, params = params_diction)
    print(flickr_resp.url)
    #savetxt(flickr_resp)
    return flickr_resp.json()

def openpic(Data):
    photos = Data['photos']['photo']
    for photo in photos:
        owner = photo['owner']
        photo_id = photo['id']
        url = 'https://www.flickr.com/photos/{}/{}'.format(owner, photo_id)
        print(url)
        webbrowser.open(url)

def savetxt(inputdata):
    File = open('/Users\FFHDXT2\Desktop\Testfile\flicrdata.txt','a')
    File.write(inputdata)
    File.close()



result_river_mts = get_flickr_data("Rocker")
openpic(result_river_mts)
