from pyModbusTCP.client import ModbusClient
import time
import struct
min_voltage_input = 0

while min_voltage_input !=-1:
    min_voltage_input_str = input("what is the min Voltage in this System ?")
    min_voltage_input = int(min_voltage_input_str)
    if min_voltage_input == -1:
        break
    def under_voltage_alarm(min_voltage,L12,L23,L13):
        print("min Voltage is:", min_voltage)
        if L12 <= min_voltage:
            return True
        if L23 <= min_voltage:
            return True
        if L13 <= min_voltage:
            return True
        else:
            return False
        print("\n")
    def get_voltage_data(ip,port):
        
        voltage_dic = {'L12':0,'L23':0,'L13':0}

        c = ModbusClient()
        c.host(ip)
        c.port(port)
        
        if not c.is_open():
                if not c.open():
                    print("unable to connect to "+SERVER_HOST+":"+str(SERVER_PORT))

        if c.is_open():
                voltage_12 = c.read_holding_registers(7,2) #looks like [17450,50214]
                voltage_23 = c.read_holding_registers(9,2)
                voltage_13 = c.read_holding_registers(11,2)
                if voltage_12:
                    voltage1 = voltage_12[0].to_bytes(2,byteorder = "little") #looks like b"0xcf3\C"
                    voltage2 = voltage_12[1].to_bytes(2,byteorder = "little")
                    concated_voltage1 = voltage2 + voltage1
                    voltage1 = struct.unpack("f",concated_voltage1) #looks like 499.000
                    voltage1_round = int(voltage1[0])

                    voltage3 = voltage_23[0].to_bytes(2,byteorder = "little")
                    voltage4 = voltage_23[1].to_bytes(2,byteorder = "little")
                    concated_voltage2 = voltage4 + voltage3
                    voltage2 = struct.unpack("f",concated_voltage2)
                    voltage2_round = int(voltage2[0])

                    voltage5 = voltage_13[0].to_bytes(2,byteorder = "little")
                    voltage6 = voltage_13[1].to_bytes(2,byteorder = "little")
                    concated_voltage3 = voltage6 + voltage5
                    voltage3 = struct.unpack("f",concated_voltage3)
                    voltage3_round = int(voltage3[0])


                    if "L12" not in voltage_dic:
                        voltage_dic["L12"] = 0
                    else:
                        voltage_dic["L12"] = voltage1_round

                    if "L23" not in voltage_dic:
                        voltage_dic["L23"] = 0
                    else:
                        voltage_dic["L23"] = voltage2_round

                    if "L13" not in voltage_dic:
                        voltage_dic["L13"] = 0
                    else:
                        voltage_dic["L13"] = voltage3_round
        
        return voltage_dic 
        
    # get Voltage
    voltagenow = get_voltage_data("10.20.203.110",502)
    for voltage in voltagenow:
        print(voltage, "has" , voltagenow[voltage] ,"V")
    

    #get Voltage min Error
    undervoltage_detection = under_voltage_alarm(min_voltage_input,voltagenow["L12"], voltagenow["L23"], voltagenow["L13"])
    print("under Voltage Alarm is:", undervoltage_detection)
    print("\n")

    

