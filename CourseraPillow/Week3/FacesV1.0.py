import zipfile
import PIL
from PIL import Image
from PIL import ImageDraw
import pytesseract
from pytesseract import image_to_string
import cv2 as cv
import numpy as np
File = open('C:/GIT/Python_Collection/CourseraPillow/Week3/faces.txt','w')

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'

mainpath = "C:/GIT/Python_Collection/CourseraPillow/Week3/"
back = '/'
archive = zipfile.ZipFile('C:/GIT/Python_Collection/CourseraPillow/Week3/small_img.zip')

face_cascade = cv.CascadeClassifier('C:/GIT/Python_Collection/CourseraPillow/Week3/haarcascade_frontalface_default.xml')

for file in archive.namelist():
    newpath = mainpath+file
    paper = archive.read(file)

    image = Image.open(newpath)
    image = image.convert('L')


    text = pytesseract.image_to_string(image)

    File.write(text)
    File.close()

    wordscount = 0
    for line in text:#
        words = line.split()
        for word in words:
            wordscount = wordscount + 1
    print(wordscount)

    if 'HOUSE' in text:
        print('found string in Text and Start Programm')
        img = cv.imread('C:/GIT/Python_Collection/CourseraPillow/Week3/test2.png')
        #img = np.full((100,80,3), 12, dtype = np.uint8)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray)

        pil_img=Image.fromarray(gray,mode="L")
        drawing=ImageDraw.Draw(pil_img)

        #print(rec) #faces.tolist()[0]
        #drawing.rectangle((faces[0][0],faces[0][1]), outline="white")
        for face in faces:
            print(face)
            drawing.rectangle([face[0],face[1],face[0]+face[2],face[1]+face[3]], outline="red")
            print('Found Face here')

        pil_img.show()

    else:
        print('not found string in Text')
    break
