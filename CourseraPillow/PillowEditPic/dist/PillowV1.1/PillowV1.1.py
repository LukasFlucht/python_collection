import PIL
from PIL import Image
from PIL import ImageFilter
from PIL import ImageEnhance

# User Decicion
def usersChoice(image):
    userdata = input('What would you like to do ?  \n 1:Cropphoto \n 2:Brightness \n 3:EditPhoto \n')
    if userdata == '1':
            userdatasize = ()
            userdatasize = input('How much du you want to cut out ? Example((50,0,190,150) \n')
            sizelst = userdatasize.split(',')
            sizetpl = ()
            print(sizelst)
            sizelstint = []
            for element in sizelst:
                sizelstint.append(int(element))
            print(sizelstint)
            sizetpl = tuple(sizelstint)
            print(sizetpl,image)
            cropphoto(sizetpl,image)
    elif userdata == '2':
        brightness(image)
    elif userdata == '3':
        editphoto(image)
    else:
        print('not such a Function \n')
        
# Loading Pictures
def choosePic():
    userdataphoto = input('Which Picture do you want to edit? \n 1:Glasses \n 2:Mountains \n')
    if userdataphoto == '1':
        file = 'picglasses.jpeg'
        image = Image.open(file)
        return image
    elif userdataphoto == '2':
        file = 'picmountains.jfif'
        image = Image.open(file)
        return image
    else:
        print('wrong input \n')
        return 'not found Picture'
   


def loadphoto():
    file = input('Type File name + .FileType: for Example: heart.jfif \n')
    try:
        image = Image.open(file)
        return image
    except:
        print('no file found')
        
# Photo Toool
def cropphoto(userdatasize,image):
    try:
        image_crop = image.crop(userdatasize)
        image_crop.show()
    except:
        print('out of Picture range')

def brightness(image):
    enhancer = ImageEnhance.Brightness(image)
    images = []
    hight = image.height
    for i in range(0,10):
        images.append(enhancer.enhance(i/10))
    first_image=images[0]
    contact_sheet=PIL.Image.new(first_image.mode, (first_image.width,10*first_image.height))
    current_location = 0
    for img in images:
        contact_sheet.paste(img, (0, current_location) )
        current_location=current_location+hight
    contact_sheet = contact_sheet.resize((160,900) )
    contact_sheet.show()
    save = input('Do you want to save the Picture ? yes or no \n')
    if save == 'no':
        return
    elif save == 'yes':
        contact_sheet.save("out.jpg", "JPEG", quality=80, optimize=True, progressive=True)
        print('Picture Saved \n')
    else:
        print('wrong input \n')
        return

def editphoto(image):
    while True:    
        enter = input('how would you like to see the Picture ? \n 0:Original \n 1:Blur \n 2:Emboss \n 3:Sharpen \n exit \n')
        if enter == '0':
            image.show()
        elif enter == '1':
            image_blur = image.filter(PIL.ImageFilter.BLUR)
            image_blur.show()
        elif enter =='2':
            image_emboss = image.filter(PIL.ImageFilter.EMBOSS)
            image_emboss.show()
        elif enter =='3':
            image_emboss = image.filter(PIL.ImageFilter.SHARPEN)
            image_emboss.show()
        elif enter == 'exit':
            return
        else:
            print('Function does not exist \n')

# Main       
while True:
    userdata = input('1:Load Picture \n2:Choose Picture from Libery \n')
    if userdata == '1':
        image = loadphoto()
        usersChoice(image)
    if userdata == '2':
        image = choosePic()
        if not image == 'not found Picture':
            usersChoice(image)
        else:
            print('no Picture Choosen')
  
