#import PIL
import PIL
from PIL import Image
from PIL import ImageEnhance
from PIL import ImageColor
from PIL import ImageDraw
from PIL import ImageFont

new_pixels = []
new_pixelstpl = ()

image = Image.open('C:\GIT\Python_Collection\CourseraPillow\Assasment\picmountains.jfif')
image = image.convert('RGB')

imagedata = image.getdata()

def changepixelred(imagedata,r):
    newimagedata = []
    for pixel in imagedata:
        newred = pixel[0]
        newred = newred * r
        newredint = int(newred)
        new_imagepix = (newredint,pixel[1],pixel[2])
        newimagedata.append(new_imagepix)
    newimage = Image.new(image.mode,image.size)
    newimage.putdata(newimagedata)

    font = ImageFont.truetype('arial.ttf',size=60)
    draw = ImageDraw.Draw(newimage)
    draw.text((image.size[0]-700,image.size[1]-60),'Chanel 0 intensety {}'.format(r),fill="white",font=font)

    return newimage

def changepixelgreen(imagedata,g):
    newimagedata = []
    for pixel in imagedata:
        newgreen = pixel[0]
        newgreen = newgreen * g
        newgreenint = int(newgreen)
        new_imagepix = (pixel[0],newgreenint,pixel[2])
        newimagedata.append(new_imagepix)
    newimage = Image.new(image.mode,image.size)
    newimage.putdata(newimagedata)

    font = ImageFont.truetype('arial.ttf',size=60)
    draw = ImageDraw.Draw(newimage)
    draw.text((image.size[0]-700,image.size[1]-60),'Chanel 1 intensety {}'.format(g),fill="white",font=font)

    return newimage

def changepixelblue(imagedata,b):
    newimagedata = []
    for pixel in imagedata:
        newblue = pixel[0]
        newblue = newblue * b
        newblueint = int(newblue)
        new_imagepix = (pixel[0],pixel[1],newblueint)
        newimagedata.append(new_imagepix)
    newimage = Image.new(image.mode,image.size)
    newimage.putdata(newimagedata)

    font = ImageFont.truetype('arial.ttf',size=60)
    draw = ImageDraw.Draw(newimage)
    draw.text((image.size[0]-700,image.size[1]-60),'Chanel 2 intensety {}'.format(b),fill="white",font=font)

    return newimage

images = []
for i in range(0,10):
    if i == 0:
        newpicred = changepixelred(imagedata,0.1)
        images.append(newpicred)
    elif i == 1:
        newpicred = changepixelred(imagedata,0.5)
        images.append(newpicred)
    elif i == 2:
        newpicred = changepixelred(imagedata,0.9)
        images.append(newpicred)

    if i == 3:
        newpicred = changepixelgreen(imagedata,0.1)
        images.append(newpicred)
    elif i == 4:
        newpicred = changepixelgreen(imagedata,0.5)
        images.append(newpicred)
    elif i == 5:
        newpicred = changepixelgreen(imagedata,0.9)
        images.append(newpicred)

    if i == 6:
        newpicred = changepixelblue(imagedata,0.1)
        images.append(newpicred)
    elif i == 7:
        newpicred = changepixelblue(imagedata,0.5)
        images.append(newpicred)
    elif i == 8:
        newpicred = changepixelblue(imagedata,0.9)
        images.append(newpicred)

first_image=images[0]
contact_sheet=PIL.Image.new(first_image.mode, (first_image.width*3,first_image.height*3))
x=0
y=0

for image in images:

    contact_sheet.paste(image,box=(x,y))
    if x + first_image.width == contact_sheet.width:
        x = 0
        y = y + first_image.height
    else:
        x = x + first_image.width


contact_sheet = contact_sheet.resize((int(contact_sheet.width/2),int(contact_sheet.height/2) ))
contact_sheet.show()
