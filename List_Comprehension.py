# List with Numbers
lst_num = [12,45,23,56,23,34,67,89,34,21]

# create a list that contains numbers from lst_num but only higher than =20
lst_comp = [num for num in lst_num if num >= 20]

# Print List 
print(lst_comp)


#Del list
del lst_comp[:]


# The same with a Loop
for num in lst_num:
    if num >= 20:
        lst_comp.append(num)
print(lst_comp)
