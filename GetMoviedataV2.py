
# some invocations that we use in the automated tests; uncomment these if you are getting errors and want better error messages
# get_sorted_recommendations(["Bridesmaids", "Sherlock Holmes"])
import json
import requests_with_caching

def get_movies_from_tastedive(movie_string):
    #server URL
    url = "https://tastedive.com/api/similar"
    #Parameters
    parameter_dic = {}
    parameter_dic["q"] = movie_string
    parameter_dic["type"] = "movies"
    parameter_dic["limit"] = 5
    response_data = requests_with_caching.get(url, params=parameter_dic)
    response_data_js = response_data.json()
    print(response_data.url)
    return response_data_js

#movie_dic = get_movies_from_tastedive("Tony Bennett")
def extract_movie_titles(movie_dic):
    movie_lst = []
    print(movie_dic.keys() )
    same_movies = movie_dic['Similar']['Results']
    for movie in same_movies:
        movie_lst.append(movie["Name"])
    return movie_lst

def get_related_titles(lst_of_movies):
    concated_lst = []
    single_lst = []
    for movie in lst_of_movies:
        get_data_from_tastedive = get_movies_from_tastedive(movie)
        extracted_content = extract_movie_titles(get_data_from_tastedive)
        concated_lst.append(extracted_content)
    for movie_list in concated_lst:
        for movie in movie_list:
            if movie not in single_lst:
                single_lst.append(movie)
    #return concated_lst
    return single_lst



def get_movie_data(movie_string_omd):
    #server URL
    url = "http://www.omdbapi.com/"
    #Parameters
    parameter_dic = {}
    parameter_dic["t"] = movie_string_omd
    parameter_dic["r"] = 'json'
    #parameter_dic["apikey"] = 2234234234
    response_data = requests_with_caching.get(url, params=parameter_dic)
    response_data_js = response_data.json()
    print(response_data.url)
    print(response_data_js.keys())
    return response_data_js


def get_movie_data(movie_string_omd):
    #server URL
    url = "http://www.omdbapi.com/"
    #Parameters
    parameter_dic = {}
    parameter_dic["t"] = movie_string_omd
    parameter_dic["r"] = 'json'
    #parameter_dic["apikey"] = 2234234234
    response_data = requests_with_caching.get(url, params=parameter_dic)
    response_data_js = response_data.json()
    #print(response_data_js.keys())
    return response_data_js

def get_movie_rating(movie_dic):
    print(movie_dic["Ratings"][1])
    if movie_dic["Ratings"][1]["Source"] == 'Rotten Tomatoes':
        rating = movie_dic["Ratings"][1]["Value"]
        print(rating[:2])
        rating_raw = int(rating[:2])
        print(rating_raw)
        return rating_raw
    else:
        return 0


def get_sorted_recommendations(lst_movies):
    print(lst_movies)
    reco_movies = get_related_titles(lst_movies)
    #sort_reco_movies = sorted(reco_movies, key =get_movie_rating)
    sort_reco_movies = sorted(reco_movies)
    print(reco_movies)
    return sort_reco_movies
